package am.studentx.studentx.aop;

import org.aspectj.lang.annotation.AfterThrowing;

import java.io.IOException;

public class AfterThrowingInterceptor {
    @AfterThrowing(value = "within(am.studentx.studentx.service..*")
    private void ioExceptionCatcher(IOException e) {

    }
}
