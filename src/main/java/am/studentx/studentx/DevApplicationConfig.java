package am.studentx.studentx;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySources(value = {
        @PropertySource({"classpath:development/persistence.properties"}),
        @PropertySource({"classpath:development/config.properties"})}
)
@Profile("development")
public class DevApplicationConfig extends ApplicationConfig {
    public DevApplicationConfig(Environment environment) {
        super(environment);
    }
}
