package am.studentx.studentx.service;

import am.studentx.studentx.persistence.model.Event;

import java.util.List;

public interface EventService {
    Event addEvent(Event event);
    Event deleteEvent(long eventId);
    Event loadEventById(long eventId);
    List<Event> loadEvents();
    Event updateEvent(Event event);
}
