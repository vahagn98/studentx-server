package am.studentx.studentx.service;

import am.studentx.studentx.persistence.model.Document;
import am.studentx.studentx.web.dto.document.FileDto;

import java.io.IOException;

public interface DocumentService {
    Document addDocument(Document document) throws IOException;

    Document deleteDocument(String id) throws IOException;

    Document getDocument(String id) throws IOException;
}
