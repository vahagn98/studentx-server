package am.studentx.studentx.service;

public interface LogService {
    void log(String message, String serviceName);
}
