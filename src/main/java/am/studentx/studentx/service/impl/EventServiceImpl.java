package am.studentx.studentx.service.impl;

import am.studentx.studentx.persistence.model.Event;
import am.studentx.studentx.persistence.repository.EventRepository;
import am.studentx.studentx.service.EventService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class EventServiceImpl implements EventService {
    private final EventRepository eventRepository;

    public EventServiceImpl(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public Event addEvent(Event event) {
        event.setDate(new Date());
        return eventRepository.save(event);
    }

    @Override
    public Event deleteEvent(long eventId) {
        Event event = eventRepository.getOne(eventId);
        eventRepository.deleteById(eventId);
        return event;
    }

    @Override
    public Event loadEventById(long eventId) {
        return eventRepository.getOne(eventId);
    }

    @Override
    public List<Event> loadEvents() {
        return eventRepository.findAll();
    }

    @Override
    public Event updateEvent(Event event) {
        return eventRepository.save(event);
    }
}
