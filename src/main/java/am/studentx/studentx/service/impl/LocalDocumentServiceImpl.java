package am.studentx.studentx.service.impl;

import am.studentx.studentx.persistence.model.Document;
import am.studentx.studentx.persistence.repository.DocumentRepository;
import am.studentx.studentx.service.DocumentService;
import am.studentx.studentx.util.FileUtil;
import am.studentx.studentx.web.dto.document.FileDto;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.UUID;

@Service("local")
@Transactional
public class LocalDocumentServiceImpl implements DocumentService {
    private final DocumentRepository documentRepository;

    public LocalDocumentServiceImpl(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    @Override
    public Document addDocument(Document document) throws IOException {
        FileUtil.saveFile(document.getId(), document.getBlob());
        Document savedDocument = documentRepository.save(document);
        savedDocument.setBlob(document.getBlob());
        return savedDocument;
    }

    @Override
    public Document deleteDocument(String id) throws IOException {
        Document document = getDocument(id);
        documentRepository.deleteById(id);
        document.setBlob(FileUtil.getBytesByFileId(document.getId()));
        return document;
    }

    @Override
    public Document getDocument(String id) throws IOException {
        Document document = documentRepository.getOne(id);
        document.setBlob(FileUtil.getBytesByFileId(document.getId()));
        return document;
    }
}
