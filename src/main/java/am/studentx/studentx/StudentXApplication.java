package am.studentx.studentx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication(scanBasePackages = "am.studentx.studentx")
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class StudentXApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(StudentXApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(StudentXApplication.class, args);
    }

}
