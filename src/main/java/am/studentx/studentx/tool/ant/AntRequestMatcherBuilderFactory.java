package am.studentx.studentx.tool.ant;

import org.springframework.security.web.util.matcher.*;

public class AntRequestMatcherBuilderFactory {
    private static AntRequestMatcherBuilderFactory antRequestMatcherBuilderFactory = new AntRequestMatcherBuilderFactory();

    private AntRequestMatcherBuilderFactory() {
    }

    public static AntRequestMatcherBuilderFactory getInstance() {
        return antRequestMatcherBuilderFactory;
    }

    public AntRequestMatcherBuilder begin() {
        return new AntRequestMatcherBuilder();
    }

    public static class AntRequestMatcherBuilder {
        private RequestMatcher requestMatcher;

        private AntRequestMatcherBuilder() {
        }

        public AntRequestMatcherBuilder add(String path, String method) {
            if (requestMatcher == null) {
                requestMatcher = new AntPathRequestMatcher(path, method);
            } else {
                requestMatcher = new AndRequestMatcher(new AntPathRequestMatcher(path, method), requestMatcher);
            }
            return this;
        }

        public AntRequestMatcherBuilder and(RequestMatcher andRequestMatcher) {
            if (requestMatcher == null) {
                requestMatcher = andRequestMatcher;
            } else {
                requestMatcher = new AndRequestMatcher(andRequestMatcher, requestMatcher);
            }
            return this;
        }

        public AntRequestMatcherBuilder and(AntRequestMatcherBuilder andRequestMatcher) {
            if (requestMatcher == null) {
                requestMatcher = andRequestMatcher.requestMatcher;
            } else {
                requestMatcher = new AndRequestMatcher(andRequestMatcher.requestMatcher, requestMatcher);
            }
            return this;
        }

        public AntRequestMatcherBuilder or(String path, String method) {
            if (requestMatcher == null) {
                requestMatcher = new AntPathRequestMatcher(path, method);
            } else {
                requestMatcher = new OrRequestMatcher(new AntPathRequestMatcher(path, method), requestMatcher);
            }
            return this;
        }

        public AntRequestMatcherBuilder or(AntRequestMatcherBuilder orRequestMatcher) {
            if (requestMatcher == null) {
                requestMatcher = orRequestMatcher.requestMatcher;
            } else {
                requestMatcher = new OrRequestMatcher(orRequestMatcher.requestMatcher, requestMatcher);
            }
            return this;
        }

        public AntRequestMatcherBuilder or(RequestMatcher orRequestMatcher) {
            if (requestMatcher == null) {
                requestMatcher = orRequestMatcher;
            } else {
                requestMatcher = new OrRequestMatcher(orRequestMatcher, requestMatcher);
            }
            return this;
        }

        public AntRequestMatcherBuilder not(RequestMatcher requestMatcher) {
            this.requestMatcher = new NegatedRequestMatcher(requestMatcher);
            return this;
        }

        public AntRequestMatcherBuilder not(AntRequestMatcherBuilder requestMatcher) {
            this.requestMatcher = new NegatedRequestMatcher(requestMatcher.requestMatcher);
            return this;
        }

        public RequestMatcher build() {
            if (requestMatcher == null) {
                return (request) -> true;
            }
            return requestMatcher;
        }
    }
}
