package am.studentx.studentx.tool.convertor.impl;

import am.studentx.studentx.persistence.model.Document;
import am.studentx.studentx.tool.convertor.Converter;
import am.studentx.studentx.web.dto.document.FileDto;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component("documentConverter")
public class DocumentConverter implements Converter<Document, FileDto> {
    @Override
    public Document to(FileDto fileDto) {
        Document fileData = new Document();
        fileData.setName(fileDto.getFileName());
        fileData.setBlob(fileDto.getBlob());
        fileData.setDate(new Date());
        fileData.setId(UUID.fromString(fileData.getName() + fileData.getDate().getTime()).toString());
        return fileData;
    }

    @Override
    public FileDto from(Document document) {
        FileDto fileDto = new FileDto();
        fileDto.setId(document.getId());
        fileDto.setFileName(document.getName());
        fileDto.setBlob(document.getBlob());
        return fileDto;
    }
}
