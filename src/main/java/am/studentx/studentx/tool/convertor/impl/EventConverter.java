package am.studentx.studentx.tool.convertor.impl;

import am.studentx.studentx.persistence.model.Event;
import am.studentx.studentx.tool.convertor.Converter;
import am.studentx.studentx.web.dto.event.AttachmentDto;
import am.studentx.studentx.web.dto.event.EventDto;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component("eventConverter")
public class EventConverter implements Converter<Event, EventDto> {

    @Override
    public Event to(EventDto dto) {
        Event event = new Event();
        event.setId(dto.getId());
        event.setDocuments(dto.getAttachments()
                .stream()
                .map(AttachmentDto::getAttachment)
                .collect(Collectors.toList()));
        event.setContent(dto.getContent());
        event.setTitle(dto.getTitle());
        event.setDescription(dto.getDescription());
        event.setDate(dto.getDate());
        return event;
    }

    @Override
    public EventDto from(Event entity) {
        EventDto eventDto = new EventDto();
        eventDto.setId(entity.getId());
        eventDto.setAttachments(entity.getDocuments()
                .stream()
                .map(AttachmentDto::new)
                .collect(Collectors.toList()));
        eventDto.setContent(entity.getContent());
        eventDto.setTitle(entity.getTitle());
        eventDto.setDescription(entity.getDescription());
        eventDto.setDate(entity.getDate());
        return eventDto;
    }
}
