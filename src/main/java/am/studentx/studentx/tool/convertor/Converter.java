package am.studentx.studentx.tool.convertor;

public interface Converter<T, U> {
    T to(U dto);
    U from(T entity);
}
