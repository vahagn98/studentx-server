package am.studentx.studentx.config;

import am.studentx.studentx.security.AccessDecisionManagerImpl;
import am.studentx.studentx.tool.ant.AntRequestMatcherBuilderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.RoleHierarchyVoter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class CommonBeans {
    @Bean("requiresAuthenticationRequestMatcher")
    public RequestMatcher requiresAuthenticationRequestMatcher() {
        AntRequestMatcherBuilderFactory.AntRequestMatcherBuilder antRequestMatcherBuilder = AntRequestMatcherBuilderFactory.getInstance().begin();
        return antRequestMatcherBuilder
                .not(antRequestMatcherBuilder
                        .add("/login", "POST")
                        .or("/public/**", "GET")
                        .or("/ui/**", "GET"))
                .build();
    }

//    @Bean
    public AccessDecisionManager accessDecisionManager() {
        return new AccessDecisionManagerImpl(
                getAllAccessDecisionVoters()
        );
    }

    private List<AccessDecisionVoter<?>> getAllAccessDecisionVoters() {
        return new ArrayList<AccessDecisionVoter<?>>() {{
            add(new WebExpressionVoter());
            add(new AuthenticatedVoter());
//            add(new RoleVoter());
            add(new RoleHierarchyVoter((authorities) -> {
                if (authorities.contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
                    List<GrantedAuthority> grantedAuthorities = new ArrayList<>(authorities);
                    grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
                    return grantedAuthorities;
                }
                return authorities;
            }));
        }};
    }
}
