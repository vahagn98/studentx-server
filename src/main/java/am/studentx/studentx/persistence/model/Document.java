package am.studentx.studentx.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;

@Entity(name = "document")
public class Document {
    @Id
    @Column(unique = true, nullable = false, name = "id")
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "date")
    private Date date;

    @Column(name = "url")
    private String url;

    @Column(name = "is_remote")
    private boolean isRemote;

    @Transient
    private byte[] blob;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isRemote() {
        return isRemote;
    }

    public void setRemote(boolean remote) {
        isRemote = remote;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public byte[] getBlob() {
        return blob;
    }

    public void setBlob(byte[] blob) {
        this.blob = blob;
    }
}
