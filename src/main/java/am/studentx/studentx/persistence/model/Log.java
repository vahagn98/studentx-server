package am.studentx.studentx.persistence.model;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "log")
@Table
public class Log {
    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "message", nullable = false)
    private String message;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "date", nullable = false)
    private Date insertionDate;

    @Column(name = "service_name")
    private String serviceName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getInsertionDate() {
        return insertionDate;
    }

    public void setInsertionDate(Date insertionDate) {
        this.insertionDate = insertionDate;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
