package am.studentx.studentx.persistence.model;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Date;

public class Token {
    private String username;
    private Collection<? extends GrantedAuthority> authorities;
    private String token;
    private Date expiredDate;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }
}
