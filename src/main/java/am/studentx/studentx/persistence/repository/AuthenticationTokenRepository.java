package am.studentx.studentx.persistence.repository;

import am.studentx.studentx.persistence.model.Token;

public interface AuthenticationTokenRepository {

    Token findUserByToken(String authenticationToken);

    Token findTokenByUsername(String username);

    void saveUserToken(Token token);

    boolean deleteUserTokensByUsername(String username);

    boolean deleteUserTokenByToken(String tokenValue);
}
