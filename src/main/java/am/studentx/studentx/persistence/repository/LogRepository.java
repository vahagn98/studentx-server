package am.studentx.studentx.persistence.repository;

import am.studentx.studentx.persistence.model.Log;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogRepository extends JpaRepository<Log, Long> {
}
