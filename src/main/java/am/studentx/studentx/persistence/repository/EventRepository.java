package am.studentx.studentx.persistence.repository;

import am.studentx.studentx.persistence.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<Event, Long> {
}
