package am.studentx.studentx.persistence.repository;

import am.studentx.studentx.persistence.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentRepository extends JpaRepository<Document, String> {
}
