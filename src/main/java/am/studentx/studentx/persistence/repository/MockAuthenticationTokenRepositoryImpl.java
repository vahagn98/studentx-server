package am.studentx.studentx.persistence.repository;

import am.studentx.studentx.persistence.model.Token;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MockAuthenticationTokenRepositoryImpl implements AuthenticationTokenRepository {
    private List<Token> tokens = new ArrayList<>();

    @Override
    public Token findUserByToken(@NonNull String authenticationToken) {
        return tokens.stream().filter(user -> user.getToken().equals(authenticationToken)).findFirst().orElse(null);
    }

    @Override
    public Token findTokenByUsername(@NonNull String username) {
        return tokens.stream().filter(user -> user.getUsername().equals(username)).findFirst().orElse(null);
    }

    @Override
    public void saveUserToken(@NonNull Token token) {
        tokens.add(token);
    }

    @Override
    public boolean deleteUserTokensByUsername(@NonNull String username) {
        return tokens.removeIf(token -> token.getUsername().equals(username));
    }

    @Override
    public boolean deleteUserTokenByToken(@NonNull String tokenValue) {
        return tokens.removeIf(token -> token.getToken().equals(tokenValue));
    }
}
