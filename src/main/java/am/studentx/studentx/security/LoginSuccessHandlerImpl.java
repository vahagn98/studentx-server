package am.studentx.studentx.security;

import am.studentx.studentx.persistence.model.Token;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class LoginSuccessHandlerImpl implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.addHeader("Content-Type", "application/json");
        Token principal = (Token) authentication.getPrincipal();
        PrintWriter writer = response.getWriter();
        writer.append("{\"access_token\":\"")
                .append(principal.getToken())
                .append("\",\"token_type\":\"bearer\",\"expires_in\":\"")
                .append(String.valueOf(principal.getExpiredDate()))
                .append("\"}");
        writer.flush();
        writer.close();
    }
}
