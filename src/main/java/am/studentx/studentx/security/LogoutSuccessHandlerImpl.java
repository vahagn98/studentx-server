package am.studentx.studentx.security;

import am.studentx.studentx.persistence.repository.AuthenticationTokenRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler {
    private final AuthenticationTokenRepository authenticationTokenRepository;

    public LogoutSuccessHandlerImpl(AuthenticationTokenRepository authenticationTokenRepository) {
        this.authenticationTokenRepository = authenticationTokenRepository;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        String authorization = request.getHeader("Authorization");
        if (authorization != null && authorization.startsWith("Bearer ")) {
            String token = authorization.substring("Bearer ".length());
            if (!authenticationTokenRepository.deleteUserTokenByToken(token)) {
                authenticationTokenRepository.deleteUserTokensByUsername(authentication.getPrincipal().toString());
            }
            return;
        }
        authenticationTokenRepository.deleteUserTokensByUsername(authentication.getPrincipal().toString());
    }
}
