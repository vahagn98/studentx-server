package am.studentx.studentx.security.filter;

import am.studentx.studentx.persistence.model.Token;
import am.studentx.studentx.persistence.repository.AuthenticationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

//@Component
public class TokenBasedAuthorizationFilter extends GenericFilterBean {
    private final AuthenticationTokenRepository authenticationTokenRepository;
    private final RequestMatcher requiresAuthenticationRequestMatcher;

    @Autowired
    public TokenBasedAuthorizationFilter(AuthenticationTokenRepository authenticationTokenRepository,
                                         @Qualifier("requiresAuthenticationRequestMatcher") RequestMatcher requiresAuthenticationRequestMatcher) {
        this.authenticationTokenRepository = authenticationTokenRepository;
        this.requiresAuthenticationRequestMatcher = requiresAuthenticationRequestMatcher;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        if (requiresAuthentication(request, response)) {
            checkToken(request, response);
        }

        chain.doFilter(request, response);
    }

    private void checkToken(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        String authenticationHeader = request.getHeader("Authorization");
        if (authenticationHeader != null) {
            if (!authenticationHeader.startsWith("Bearer ")) {
                throw new BadCredentialsException("Full authentication is required to access this resource.");
            }
            authenticationHeader = authenticationHeader.substring("Bearer ".length());
            Token token = authenticationTokenRepository.findUserByToken(authenticationHeader);
            if (token == null || token.getExpiredDate().getTime() < new Date().getTime()) {
                SecurityContextHolder.getContext().setAuthentication(null);
                throw new BadCredentialsException("Token is invalid.");
            }
            return;

        }
        throw new BadCredentialsException("Full authentication is required to access this resource.");
    }

    private boolean requiresAuthentication(HttpServletRequest request,
                                           HttpServletResponse response) {
        return requiresAuthenticationRequestMatcher.matches(request);
    }
}
