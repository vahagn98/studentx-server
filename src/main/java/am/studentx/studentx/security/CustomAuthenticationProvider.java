package am.studentx.studentx.security;

import am.studentx.studentx.persistence.model.Token;
import am.studentx.studentx.persistence.repository.AuthenticationTokenRepository;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
    private final AuthenticationTokenRepository authenticationTokenRepository;

    public CustomAuthenticationProvider(AuthenticationTokenRepository authenticationTokenRepository) {
        this.authenticationTokenRepository = authenticationTokenRepository;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) authentication;
        String username = usernamePasswordAuthenticationToken.getPrincipal().toString();
        String password = usernamePasswordAuthenticationToken.getCredentials().toString();
//        Token tokenByUsername = authenticationTokenRepository.findTokenByUsername(username);
//        if (tokenByUsername != null) {
//            return new UsernamePasswordAuthenticationToken(tokenByUsername, password, authentication.getAuthorities());
//        }
        if ("admin".equalsIgnoreCase(username) && "admin".equalsIgnoreCase(password)) {
            Token token = getUser(username);
            token.setAuthorities(Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN")));
            authenticationTokenRepository.saveUserToken(token);
            return new UsernamePasswordAuthenticationToken(token, password, token.getAuthorities());
        }
        if ("user".equalsIgnoreCase(username) && "user".equalsIgnoreCase(password)) {
            Token token = getUser(username);
            token.setAuthorities(Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));
            authenticationTokenRepository.saveUserToken(token);
            return new UsernamePasswordAuthenticationToken(token, password, token.getAuthorities());
        }
        throw new BadCredentialsException("Bad credentials");
    }

    private Token getUser(String username) {
        UUID uuid = UUID.randomUUID();
        Token token = new Token();
        LocalDateTime localDateTime = LocalDateTime.now(ZoneId.systemDefault());
        localDateTime = localDateTime.plusHours(3);
        token.setExpiredDate(Date.from(localDateTime.atZone(ZoneOffset.systemDefault()).toInstant()));
        token.setUsername(username);
        token.setToken(uuid.toString());
        return token;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.isAssignableFrom(UsernamePasswordAuthenticationToken.class);
    }
}
