package am.studentx.studentx;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySources(value = {
        @PropertySource({"classpath:production/persistence.properties"}),
        @PropertySource({"classpath:production/config.properties"})}
)
@Profile("!development")
public class ProdApplicationConfig extends ApplicationConfig {
    public ProdApplicationConfig(Environment environment) {
        super(environment);
    }
}
