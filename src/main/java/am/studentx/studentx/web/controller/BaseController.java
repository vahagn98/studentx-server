package am.studentx.studentx.web.controller;

import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
public class BaseController {
}
