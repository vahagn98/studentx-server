package am.studentx.studentx.web.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class LoginController extends BaseController {

    @GetMapping(path = "/log")
    @PreAuthorize("#oauth2.hasScope('read')")
    public String login() {
        return "{\"ok\":\"OK\"}";
    }

    @PostMapping(path = "/admin")
    @PreAuthorize("#oauth2.hasScope('write')")
//    @Secured("ROLE_ADMIN")
    public String admin() {
        return "{\"ok\":\"admin\"}";
    }
}
