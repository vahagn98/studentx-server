package am.studentx.studentx.web.controller;

import am.studentx.studentx.persistence.model.Document;
import am.studentx.studentx.service.DocumentService;
import am.studentx.studentx.tool.convertor.Converter;
import am.studentx.studentx.util.FileUtil;
import am.studentx.studentx.web.dto.document.FileDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;

@RestController
@RequestMapping(path = "/document")
@MultipartConfig
public class DocumentController extends BaseController {
    private final DocumentService documentService;
    private final Converter<Document, FileDto> converter;

    public DocumentController(DocumentService documentService,
                              @Qualifier("documentConverter") Converter<Document, FileDto> converter) {
        this.documentService = documentService;
        this.converter = converter;
    }

    @PostMapping
    public FileDto addFile(@RequestParam("file") MultipartFile file) throws IOException {
        FileDto fileDto = new FileDto();
        fileDto.setFileName(file.getName());
        fileDto.setBlob(FileUtil.getInputStreamBytes(file.getInputStream()));
        Document fileData = converter.to(fileDto);
        Document document = documentService.addDocument(fileData);
        return converter.from(document);
    }

    @GetMapping(path = "/{id}")
    public FileDto getDocument(@PathVariable String id) throws IOException {
        Document document = documentService.getDocument(id);
        return  converter.from(document);
    }

    @DeleteMapping(path = "/{id}")
    public FileDto deleteDocument(@PathVariable String id) throws IOException {
        Document document = documentService.deleteDocument(id);
        FileDto fileDto =  converter.from(document);
        FileUtil.deleteFileByFileId(document.getId());
        return fileDto;
    }

}
