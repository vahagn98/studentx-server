package am.studentx.studentx.web.controller;

import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

@RestController
public class IndexController extends BaseController {
    private String indexHtml = null;

    @RequestMapping(value={"/ui", "/ui/", "/ui/login"})
    public String getIndexHtml() {

        if (indexHtml != null) return indexHtml;

        try (InputStream is = new ClassPathResource("ui/index.html").getInputStream();
             BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            indexHtml = br.lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return indexHtml;
    }
}
