package am.studentx.studentx.web.controller;

import am.studentx.studentx.persistence.model.Event;
import am.studentx.studentx.service.EventService;
import am.studentx.studentx.tool.convertor.Converter;
import am.studentx.studentx.web.dto.event.EventDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "event")
public class EventController extends BaseController {
    private final EventService eventService;
    private final Converter<Event, EventDto> converter;

    public EventController(EventService eventService,
                           @Qualifier("eventConverter") Converter<Event, EventDto> converter) {
        this.eventService = eventService;
        this.converter = converter;
    }

    @GetMapping(path = "{id}")
    public EventDto loadEvent(@PathVariable("id") Long eventId) {
        Event event = eventService.loadEventById(eventId);
        return converter.from(event);
    }

    @PostMapping
    public EventDto saveEvent(@RequestBody EventDto eventDto) {
        //TODO validate
        Event event = converter.to(eventDto);
        Event savedEvent = eventService.addEvent(event);
        return converter.from(savedEvent);
    }

    @PutMapping(path = "{id}")
    public EventDto updateEvent(@RequestBody EventDto eventDto, @PathVariable("id") Long eventId) {
        //TODO validate
        Event event = converter.to(eventDto);
        Event updatedEvent = eventService.updateEvent(event);
        return converter.from(updatedEvent);
    }

    @DeleteMapping(path = "{id}")
    public EventDto deleteEvent(@PathVariable(value = "id") Long eventId) {
        return converter.from(eventService.deleteEvent(eventId));
    }

    @GetMapping
    public List<EventDto> loadEvents() {
        return eventService.loadEvents().stream()
                .map(converter::from)
                .collect(Collectors.toList());
    }
}
