package am.studentx.studentx.web.dto.event;

public class AttachmentDto {
    private String attachment;

    public AttachmentDto() {
    }

    public AttachmentDto(String attachment) {
        this.attachment = attachment;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }
}
