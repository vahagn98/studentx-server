package am.studentx.studentx.util;

import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtil {
    private FileUtil() {}

    public static byte[] getInputStreamBytes(InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        Reader reader = new InputStreamReader(inputStream);
        char[] blob = new char[1024];
        int length;
        while ((length = reader.read(blob, 0, 1024)) != -1) {
            stringBuilder.append(blob, 0, length);
        }
        return stringBuilder.toString().getBytes();
    }

    public static byte[] getBytesByFileId(String id) throws IOException {
        return Files.readAllBytes(Paths.get(new ClassPathResource(id).getPath()));
    }

    public static void deleteFileByFileId(String id) throws IOException {
        Files.deleteIfExists(Paths.get(new ClassPathResource(id).getPath()));
    }

    public static void saveFile(String id, byte[] blob) throws IOException {
        File file = new ClassPathResource(id).getFile();
        Files.write(Paths.get(file.getPath()), blob);
    }
}
